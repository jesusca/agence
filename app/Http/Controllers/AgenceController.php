<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\LoginRequest;
use Auth;
use App\Grupo;
use App\Contacto;
use App\Cao_usuario;
use App\Cao_salario;
use App\Cao_factura;
use App\Cao_os;
use Redirect; 
use Storage;
use DB;
use Session;

class AgenceController extends Controller
{



    public function index(){

     $user = DB::table('permissao_sistema')->join('cao_usuario',
        'permissao_sistema.co_usuario', '=','cao_usuario.co_usuario')
     ->select('cao_usuario.no_usuario', 'cao_usuario.co_usuario')
     ->whereIn('permissao_sistema.co_tipo_usuario' , array(0,1,2))
     ->where('permissao_sistema.co_sistema' , 1)
     ->where('permissao_sistema.in_ativo', '=', 'S')->get();

     return view('agence.comercial',compact('user'));
 }



 public function login(){

  return view('login');

}

public function consulta_grafica(Request $request){

    $desde= $request->desde;
    $hasta= $request->hasta;
    $consultor= $request->consultor;
    $boton1= $request->boton1;

    if ($boton1==1) {

        $query1 = DB::table('cao_os')
        ->join('cao_salario','cao_os.co_usuario', '=','cao_salario.co_usuario')
        ->join('cao_usuario','cao_os.co_usuario', '=','cao_usuario.co_usuario')
        ->join('cao_fatura','cao_os.co_os', '=','cao_fatura.co_os')
        ->select('cao_usuario.no_usuario',DB::raw("SUM(cao_fatura.valor-(cao_fatura.valor*(cao_fatura.total_imp_inc/100))) as total"),
            DB::raw("SUM(cao_fatura.valor-(cao_fatura.total_imp_inc/100))*(cao_fatura.comissao_cn/100) as otro"),'cao_salario.brut_salario')
        ->whereIn('cao_os.co_usuario', $consultor)
        ->whereBetween('data_emissao', [$desde, $hasta])
        ->groupBy(DB::raw('MONTH(data_emissao)'),'cao_usuario.no_usuario','cao_salario.brut_salario' )
        ->orderBy('cao_usuario.no_usuario', 'ASC')
        ->orderBy(DB::raw('MONTH(data_emissao)'))
        ->get();

        return view('agence.grafico',compact('query1','desde','hasta'));

    }


    if ($boton1==2) {

        $query1 = DB::table('cao_os')
        ->join('cao_usuario','cao_os.co_usuario', '=','cao_usuario.co_usuario')
        ->join('cao_fatura','cao_os.co_os', '=','cao_fatura.co_os')
        ->select('cao_usuario.no_usuario',DB::raw("SUM(cao_fatura.valor-(cao_fatura.valor*(cao_fatura.total_imp_inc/100))) as total"))
        ->whereIn('cao_os.co_usuario', $consultor)
        ->whereBetween('data_emissao', [$desde, $hasta])
        ->groupBy('cao_usuario.no_usuario',DB::raw('MONTH(data_emissao)'))
        ->get();



        return view('agence.grafico2',compact('query1'));
    }
    

    if ($boton1==3) {

        $query1 = DB::table('cao_os')
        ->join('cao_usuario','cao_os.co_usuario', '=','cao_usuario.co_usuario')
        ->join('cao_fatura','cao_os.co_os', '=','cao_fatura.co_os')
        ->select('cao_usuario.no_usuario',DB::raw("SUM(cao_fatura.valor-(cao_fatura.valor*(cao_fatura.total_imp_inc/100))) as total"))
        ->whereIn('cao_os.co_usuario', $consultor)
        ->whereBetween('data_emissao', [$desde, $hasta])
        ->groupBy('cao_usuario.no_usuario')
        ->get();

        return view('agence.grafico1',compact('query1'));
    }

    return view('login');

}




















public function store(Request $request)
{
        //
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
