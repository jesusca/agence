<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class Permissao_sistema extends Model
{
    protected $table = 'permissao_sistema';
   	protected $fillable = ['co_usuario', 'co_tipo_usuario', 'co_sistema', 'in_ativo', 'co_usuario_atualizacao', 'dt_atualizacao'];
    public $timestamps = false;
}
