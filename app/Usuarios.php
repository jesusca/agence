<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuarios extends Model
{
    protected $table = 'users';
   	protected $fillable = ['id','plan', 'tipo_user', 'nombre', 'apellido', 'cedula', 'email', 'username',
   	 'password', 'telefono', 'institucion_eclesiastica_id'];
    public $timestamps = false;



    protected $hidden = ['password', 'remember_token'];
    public function setPasswordAttribute($valor){
       if(!empty($valor)){
          $this->attributes['password'] = \Hash::make($valor);
       }
    }
}
