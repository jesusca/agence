var socket = io('http://chatemp.herokuapp.com');

var app = angular.module("app",[],function($compileProvider) {
    $compileProvider.directive('compile', function($compile) {
		return function(scope, element, attrs) {
			scope.$watch(
				function(scope) {
					return scope.$eval(attrs.compile);
				},
				function(value) {
					element.html(value);
					$compile(element.contents())(scope);
				}
			);
		};
    }); 
});

app.controller("DirectoriosController",['$http','$scope','$location', directorios]);
app.controller("ContabilidadController",['$http','$scope','$location', contabilidad]);
function directorios($http,$scope){
	$scope.placeholder = 'Buscar Contactos';
	$http.post('/cargar_contactos').
	success(function(data){
		$scope.contacts = data;
	});
	$http.post('/cargar_perfil').
	success(function(data){
		$scope.perfil = data[0];
		$scope.telefono_array = data[1];
			$scope.email_array = data[2];
		if(data!=""){
			if($scope.perfil[0].imagen!=''){
				$(".img_perfil").attr('src','imagenes/'+$scope.perfil[0].imagen);
			}else{
				$(".img_perfil").attr('src','images/a3.jpg');
			}
		}
	});
	$http.post('/cargar_grupo').
	success(function(data){
		$scope.grupo = data;
	});
	$scope.contacto_grupo = function(id_grupo){
		$http.post('/cargar_contactos',{'id':id_grupo}).
		success(function(data){
			$scope.contacts = data;
		});
	}
	$scope.new_contact = function(){
        $('#accion').fadeIn();
    	$(".perfil").css("overflow", "auto");
    	$(".perfil").css("display", "none");
    	$(".perfil_guardar").css("display", "block");
    	$(".perfil_actualizar").css("display", "none");
    	$(".img_perfil").attr('src','images/a3.jpg');
    	$scope.accion = '<a class="btn btn-default btn-xs" onclick="guardar_contacto()"  href="">Guardar</a><a class="btn btn-danger btn-xs cancelar"  ng-click="cancelar()" href="#">Cancelar</a>';
		
	}
	$scope.cancelar = function(){
	    $scope.accion = '<a class="btn btn-default btn-xs" ng-click="editar_contacto()" href="">Editar</a>';
	    $(".perfil").css("display", "block");
	    $(".perfil_guardar").css("display", "none");
	    if($scope.perfil[0].imagen!=''){
			$(".img_perfil").attr('src','imagenes/'+$scope.perfil[0].imagen);
		}else{
			$(".img_perfil").attr('src','images/a3.jpg');
		}
	    $("#formulario")[0].reset();
	}
	$scope.contacto_perfil = function(id){

		$http.post('/cargar_perfil',{'id':id}).
		success(function(data){
			$('#accion').fadeIn();
			$scope.perfil = data[0];
			$scope.telefono_array = data[1];
			$scope.email_array = data[2];
			$scope.titulo_array = data[3];
			$scope.cargo_array = data[4];
			if($scope.perfil[0].imagen!=''){
				$(".img_perfil").attr('src','imagenes/'+$scope.perfil[0].imagen);
				
			}else{
				$(".img_perfil").attr('src','images/a3.jpg');
			}
			
			$scope.cancelar_editar();
			$(".perfil_guardar").css("display", "none");
		});
	}
	$scope.editar_contacto = function(){
		$(".perfil").css("overflow", "auto");
    	$(".perfil").css("display", "none");
    	$(".perfil_actualizar").css("display", "block");
    	var fechaArray = $scope.perfil[0].fecha_nac.split('-');
    	var ano = parseInt(fechaArray[0]);
    	var mes = fechaArray[1];
    	var dia = parseInt(fechaArray[2]);
    	var fecha = new Date(ano+'/'+mes+'/'+dia);
    	var fechaGradArray = $scope.perfil[0].fecha_graduacion.split('-');
    	var anoG = parseInt(fechaGradArray[0]);
    	var mesG = fechaGradArray[1];
    	var diaG = parseInt(fechaGradArray[2]);
    	var fechaGrad = new Date(anoG+'/'+mesG+'/'+diaG);
    	$scope.accion = '<a class="btn btn-default btn-xs" onclick="actualizar_contacto()"  href="">Actualizar</a><a class="btn btn-danger btn-xs cancelar"  ng-click="cancelar_editar()" href="#">Cancelar</a>';
		$scope.nombre=$scope.perfil[0].nombre;
		$scope.sexo=$scope.perfil[0].sexo;
		var option_sexo = `	<option selected value="${$scope.sexo}">
								${$scope.sexo}
							</option>
							<option value="Masculino">Masculino</option>
							<option value="Femenino">Femenino</option>`;
		$('#select_sexo')[0].innerHTML=option_sexo;
		$scope.tipo_doc=$scope.perfil[0].tipo_doc;
		var option_tipo_doc = `	<option selected value="${$scope.tipo_doc}">
									${$scope.tipo_doc}
								</option>
								<option value="Cédula de Identidad (CI)">
									Cédula de Identidad (CI)
								</option>
								<option value="Cédula de Ciudadanía (CC)">
									Cédula de Ciudadanía (CC)
								</option>
								<option value="Tarjeta de Identidad (TI)">
									Tarjeta de Identidad (TI)
								</option>
								<option value="Registro Civil (RC)">
									Registro Civil (RC)
								</option>
								<option value="Cédula de Extranjería (CE)">
									Cédula de Extranjería (CE)
								</option>
								<option value="Carné de Identidad (CI)">
									Carné de Identidad (CI)
								</option>
								<option value="Documento Nacional de Identidad (DNI)">
									Documento Nacional de Identidad (DNI)
								</option>
								<option value="Documento Único de Identidad (DUI)">
									Documento Único de Identidad (DUI)
								</option>
								<option value="identificación oficial o identificación (ID)">
									identificación oficial o identificación (ID)
								</option>`;
		$('#select_doc')[0].innerHTML=option_tipo_doc;
		$scope.num_doc=parseInt($scope.perfil[0].num_doc);
		$scope.fecha_nac=fecha;
		$scope.cod_parroquia=$scope.perfil[0].cod_parroquia;
		$scope.cod_postal=parseInt($scope.perfil[0].cod_postal);
		$scope.telefono=$scope.telefono_array;
		$scope.correo=$scope.email_array;
		$scope.cod_fax=$scope.perfil[0].cod_fax;
		$scope.profesion=$scope.perfil[0].profesion;
		$scope.cargos=$scope.cargo_array;
		$scope.titulo=$scope.titulo_array;
		$scope.fecha_graduacion=fechaGrad;
		$scope.web=$scope.perfil[0].web;
		$scope.redes=$scope.perfil[0].redes;
		$scope.observaciones=$scope.perfil[0].observaciones;
		for(i in $scope.grupo){
			if ($scope.grupo[i].id==$scope.perfil[0].grupo) {
				$scope.grupo_id=$scope.grupo[i].id;
				$scope.perfil[0].grupo=$scope.grupo[i].grupo;
				console.log($scope.perfil[0].grupo);
			}
		}
	}
	$scope.cancelar_editar = function(){
    	$scope.accion =  '<a class="btn btn-default btn-xs" ng-click="editar_contacto()" href="">Editar</a>';
    	$(".perfil").css("display", "block");
    	$(".perfil_actualizar").css("display", "none");
    	$("#formulario")[0].reset();
	}
	$scope.remove_contact = function(elemento){
		if(confirm('Esta seguro?')){
			input = document.createElement("input");
			input.type = "hidden";
			input.name = "id";
			input.value = elemento.cont.id;
			$('#form_del').append(input);
			$('#form_del').submit();
		}else{
			return false;
		}
		
	}
}
function edit_grupo(elemento,id_param){
    id = $("#"+id_param+"");
    $(elemento).css('display','none');
    grupo = $(elemento)[0].innerText;
    id.append('<input type="text" name="grupo" value="'+grupo+'" class="form-control"><a class="btn btn-success btn-xs" style="margin-top:1%;" onclick="actualizar(this,'+id_param+')"  href="">Editar</a><a class="btn btn-danger btn-xs" style="margin-top:1%;margin-left:1%;" onclick="eliminar_grupo('+id_param+')" href="">ELiminar</a>');
}
function actualizar_contacto(){
	regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
	if(regex.test($('#correo_a').val().trim()) || $('#correo_a').val()==''){
		$('#formulario_actualizar').submit();
	}else{
		alert('Ingresó un correo invalido!!');
	}

	
}
function actualizar(param,id){
	grupo_input=$(param)[0].previousElementSibling;
	grupo_eliminar=$(param)[0].nextElementSibling;
	grupo_value = $(param)[0].previousElementSibling.value;
	a = $("#"+id+"").find('a')[0];
	$.ajax({
		headers: {
        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    	},
	    url:"update_grupo_ajax",
	    type:"POST",
	    dataType:"json",
	    data:{grupo:grupo_value,id:id},
	    success: function(dato){
			if(dato==1){
				$("#"+id+"").find('a')[0].innerText=grupo_value;
				$(param).remove();
				$(grupo_input).remove();
				$(grupo_eliminar).remove();
				$(a).css('display','block');
			}
	    }
	});
}
function eliminar_grupo(id){
	$.ajax({
		headers: {
        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    	},
	    url:"eliminar_grupo_ajax",
	    type:"POST",
	    dataType:"json",
	    data:{id:id},
	    success: function(dato){
			if(dato==1){
				$("#"+id+"").remove();
			}else if(dato==3){
				$("#alerta_grupo").fadeIn();
	        	$("#alerta_grupo").fadeOut(3000);
			}
	    }
	});
}
function load(imagen){
    var id = imagen.id;
    var input = $("#"+id+"");
    var files = input[0].files;
    
    if(files[0].size<2000000 && (files[0].type=='image/jpeg' || files[0].type=='image/png') ){
    	
    	$('#upload').text(files[0].name);
    	
    	for (var i = 0, f; f = files[i]; i++) {
        //Solo admitimos imágenes.
	        if (!f.type.match('image.*')) {
	            continue;
	        }
	        var reader = new FileReader();
	        reader.onload = (function(theFile) {
	            return function(e) {
	            // Insertamos la imagen
	            //document.getElementById("perfil_img").innerHTML = ['<img class="img_perfil" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
	            document.getElementById("perfil_img").src=e.target.result;
	            $(".img_perfil").css('height','100px');
	            $(".img_perfil").css('width','100px');
	            };
	        })(f);
	        reader.readAsDataURL(f);
    	}
    	
    	
    }else{
    	fileupload = $('#uploadBtn');  
		fileupload.replaceWith(fileupload.clone(true));
		fileupload.val("");
    	$('#upload').text('Cargar Archivo');
    	$("#perfil_img").attr('src','images/a3.jpg');
    	$("#alerta").fadeIn();
	    $("#alerta").fadeOut(3000);
    }

    
}
function contabilidad($http,$scope){

	$scope.open_save = function(){
		var tipo = $('#tipo_arancel').val();
		var pista_plan = [];
		$http.post('/cargar_plan',{tipo:tipo}).
		success(function(data){
			for(i in data){
				pista_plan.push(data[i].codigo_plan);
				pista_plan.push(data[i].descripcion);
			}
			$("#buscar_plan").autocomplete({
				lookup: pista_plan,
				onSelect: function (suggestion) {
					for(i in data){
						if(data[i].codigo_plan===suggestion.value || data[i].descripcion===suggestion.value){
							$("#codigo_plan").val(data[i].codigo_plan);
							$("#arancel_save").val(data[i].descripcion);
						}
					}	
				}
			});
		});
		$("#precio_save").maskMoney({prefix:'', allowNegative: true, thousands:'', decimal:',', affixesStay: false});
		
			
	}
	$scope.cargar_aranceles = function(){
		var tipo=3;
		var pista_arancel = [];
		var pista_plan = [];
		$http.post('/cargar_aranceles').
		success(function(data){
			$scope.arancel_find=data;
			for(i in data){
				pista_arancel.push(data[i].codigo_plan);
				pista_arancel.push(data[i].arancel);
			}
			$("#codigo").autocomplete({
				lookup: pista_arancel,
				onSelect: function (suggestion) {
					for(i in data){
						if(data[i].codigo_plan===suggestion.value || data[i].arancel===suggestion.value){
							$("#codigo").val(data[i].codigo_plan);
							$("#precio").val(data[i].precio);
							$("#cantidad").val(1);
							$("#monto").val(data[i].precio);
						}
					}	

				}
			});
		});
		$http.post('/cargar_plan',{tipo:tipo}).
		success(function(data2){
			$scope.planes_find=data2;
			for(i in data2){
				pista_plan.push(data2[i].codigo_plan);
				pista_plan.push(data2[i].descripcion);
			}
			$("#codigo_pagar").autocomplete({
				lookup: pista_plan,
				onSelect: function (s) {
					for(i in data2){
						if(data2[i].codigo_plan===s.value || data2[i].descripcion===s.value){
							$("#codigo_pagar").val(data2[i].codigo_plan)
						}
					}	

				}
			});
		}); 
	}
	$scope.save_ingreso = function() {
		var codigo = $('#codigo').val();
		var cantidad = $('#cantidad').val();
		var precio = $('#precio').val();
		var descripcion = $('#descripcion').val();
		var fecha = $('#fecha').val();
		var monto = $('#monto').val();
		var codigo_pagar = $('#codigo_pagar').val();
		var params = [codigo,cantidad,precio,descripcion,fecha,monto,codigo_pagar];
		not_empty = 0;
		for(i in params){
			if (params[i]!="") {
				not_empty+=1;
			}
		}
		if (not_empty==7) {
			$http.post('/save_ingreso',{params:params}).
			success(function(response){
				if (response==1) {
					$('#codigo').val('');
					$('#cantidad').val('');
					$('#precio').val('');
					$('#descripcion').val('');
					$('#fecha').val('');
					$('#monto').val('');
					$('#codigo_pagar').val('');
					var tr =`<tr>
								<td>
									${codigo}
								</td>
								<td>
									${cantidad}
								</td>
								<td>
									${precio}
								</td>
								<td>
									${descripcion}
								</td>
								<td>
									${fecha}
								</td>
								<td>
									${monto}
								</td>
								<td>
									${codigo_pagar}
								</td>
							</tr>`;
					$('#resultados_body').append(tr);
					$('#ingresos_body').append(tr);
					$('#ingresos_recientes').show(1500);
					Push.create('Registro Exitoso!',{
					    body: "Los datos ingresado fueron registrados exitosamente",
					    icon: '../img/title.png',
					    timeout: 4000,
					    onClick: function () {
					        window.focus();
					        this.close();
					    }
					})

				};
			});
		}else{
			Push.create('Error!',{
			    body: "Por favor ingresar todos los datos solicitados",
			    icon: '../img/title.png',
			    timeout: 4000,
			    onClick: function () {
			        window.focus();
			        this.close();
			    }
			})
		}
		
	}
	$scope.guardar_arancel = function(){
		precio = document.getElementById("precio_save").value;
		arancel = document.getElementById("arancel_save").value;
		if(precio == null || precio.length == 0 || /^\s+$/.test(precio) || arancel == null || arancel.length == 0 || /^\s+$/.test(arancel)  ) {
			$('#alerta_arancel').fadeIn();
			$("#alerta_arancel").fadeOut(3000);
		}else{
			
			$('#form_arancel').submit();
		}
	}
	$scope.editar_arancel = function(id){
		$("#precio_update").maskMoney({prefix:'', allowNegative: true, thousands:'', decimal:',', affixesStay: false});
		var tipo = $('#tipo_arancel').val();
		var pista_plan = [];
		var tr = $('#'+id);
		var arancel = tr[0].children[0].innerHTML;
		var precio = tr[0].children[1].innerHTML;
		$('#arancel_update').val(arancel);
		$('#precio_update').val(precio);
		$('#id_arancel').val(id);
		$http.post('/cargar_plan',{tipo:tipo}).
		success(function(data){
			for(i in data){
				pista_plan.push(data[i].codigo_plan);
				pista_plan.push(data[i].descripcion);
				if(data[i].descripcion===arancel){
					$("#codigo_plan_update").val(data[i].codigo_plan);
				}
			}
			$("#buscar_plan_update").autocomplete({
				lookup: pista_plan,
				onSelect: function (suggestion) {
					for(i in data){
						if(data[i].codigo_plan===suggestion.value || data[i].descripcion===suggestion.value){
							$("#codigo_plan_update").val(data[i].codigo_plan);
							$("#arancel_update").val(data[i].descripcion);
						}
					}	
				}
			});
		});
		

	}
	$scope.actualizar_arancel = function(){
		var codigo_plan = $('#codigo_plan_update').val();
		var arancel = $('#arancel_update').val();
		var precio = $('#precio_update').val();
		var id =  $('#id_arancel').val();
		if(precio == null || precio.length == 0 || /^\s+$/.test(precio) || arancel == null || arancel.length == 0 || /^\s+$/.test(arancel) ) {
			$('#alerta_arancel_update').fadeIn();
			$("#alerta_arancel_update").fadeOut(3000);
		}else{
			$.ajax({
				headers: {
		        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    	},
			    url:"actualizar_arancel",
			    type:"POST",
			    dataType:"json",
			    data:{id:id,codigo_plan:codigo_plan,arancel:arancel,precio:precio},
			    success: function(dato){
			    	$("#buscar_plan_update").val('');
					var tr = $('#'+id);
					tr[0].children[0].innerHTML = arancel;
					tr[0].children[1].innerHTML = precio;
					$('#alerta_arancel_registro').fadeIn();
					$("#alerta_arancel_registro").fadeOut(4000);
			    }
			});
		}
	}
	$scope.eliminar_arancel = function(id){
		$.ajax({
			headers: {
	        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    	},
		    url:"eliminar_arancel",
		    type:"POST",
		    dataType:"json",
		    data:{id:id},
		    success: function(dato){
		    	var tr = $('#'+id);
		    	if(dato==1){
		    		tr.remove();
		    		$('#alerta_arancel_registro').fadeIn();
					$("#alerta_arancel_registro").fadeOut(4000);
		    	}
				

		    }
		});
	}
}
function load_actualizar(imagen){
    var id = imagen.id;
    var input = $("#"+id+"");
    var files = input[0].files;
    if(files[0].size<2000000 && (files[0].type=='image/jpeg' || files[0].type=='image/png') ){
    
	    $('#upload_actualizar').text(files[0].name);
	    for (var i = 0, f; f = files[i]; i++) {
	        //Solo admitimos imágenes.
	        if (!f.type.match('image.*')) {
	            continue;
	        }
	        var reader = new FileReader();
	        reader.onload = (function(theFile) {
	            return function(e) {
	            // Insertamos la imagen
	            //document.getElementById("perfil_img").innerHTML = ['<img class="img_perfil" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
	            document.getElementById("perfil_img_actualizar").src=e.target.result;
	            $(".img_perfil").css('height','100px');
		        $(".img_perfil").css('width','100px');
	            };
	        })(f);
	        reader.readAsDataURL(f);
	    }
	}else{
		fileupload = $('#uploadBtn');  
		fileupload.replaceWith(fileupload.clone(true));
		fileupload.val("");
    	$('#upload_actualizar').text('Cargar Archivo');
    	$("#perfil_img_actualizar").attr('src','images/a3.jpg');
    	$("#alerta").fadeIn();
	    $("#alerta").fadeOut(3000);
	}
}
function guardar_contacto(){
	var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
	if($("#nombre_save").val()!='' && $("#num_doc_save").val()!='' && (regex.test($('#correo').val().trim()) || $('#correo').val()=='')){
		$('#formulario').submit();	
	}else{
		alert('Existen Campos Sin llenar o Ingresó un correo invalido!!');
	}
}
function selected(li){
	$(li).parent().css('background-color','#eeeeee');
	
}
function show_remove(li){
	$(li)[0].childNodes[1].style.display='block';
} 
function show_remove_disabled(li){
	$(li)[0].childNodes[1].style.display='none';
} 
function unselected(li){
	$(li).parent().css('background-color','white');
}
function add(id){
	if (id===1 || id===3){
		input = `<input name="telefono[]" class="form-control add">
				<a href="" onclick="remove_add(this)" >
					<i class="fa fa-times remove_add" aria-hidden="true"></i>
				</a>`;
		if(id===1){
			$('#telefono').append(input);
		}else if(id===3){
			$('#telefono_actualizar').append(input);
		}
		
	}else if(id==2 || id===4){
		input = `<input name="correo[]" class="form-control add">
				<a href="" onclick="remove_add(this)" >
					<i class="fa fa-times remove_add" aria-hidden="true"></i>
				</a>`;
		
		if(id===2){
			$('#email').append(input);
		}else if(id===4){
			$('#email_actualizar').append(input);
		}
	}else if(id==5 || id===6){
		input = `<input name="titulos[]" class="form-control add">
				<a href="" onclick="remove_add(this)" >
					<i class="fa fa-times remove_add" aria-hidden="true"></i>
				</a>`;
		
		if(id===5){
			$('#titulos').append(input);
		}else if(id===6){
			$('#titulos_actualizar').append(input);
		}
	}else if(id==7 || id===8){
		input = `<input name="cargo[]" class="form-control add">
				<a href="" onclick="remove_add(this)" >
					<i class="fa fa-times remove_add" aria-hidden="true"></i>
				</a>`;
		
		if(id===7){
			$('#cargos').append(input);
		}else if(id===8){
			$('#cargos_actualizar').append(input);
		}
	}
}
function remove_add(elemento){
	input = $(elemento)[0].previousElementSibling;
	input.remove();
	$(elemento).remove();
}
function zoom(elemento,efecto){
	var h1 = $(elemento)[0].children[0].children[0];
	if (efecto===1) {
		$(h1).css("transform", "scale(1.2)");
	    $(h1).css("-moz-transform", "scale(1.2)");
	    $(h1).css("-webkit-transform", "scale(1.2)");
	    $(h1).css("-o-transform", "scale(1.2)");
	}else if (efecto===2) {
		$(h1).css("transform", "scale(1)");
	    $(h1).css("-moz-transform", "scale(1)");
	    $(h1).css("-webkit-transform", "scale(1)");
	    $(h1).css("-o-transform", "scale(1)");
	};
    

};
function calcular_monto(){
	var precio = $('#precio').val();
	var cantidad = $('#cantidad').val();

	if (cantidad=='') {
		cantidad=1
	};
	var precio_clean = precio.replace(".","").replace(",",".");
	var total = cantidad * precio_clean;
	var monto_total = formatear_monto(total,2);
	$("#monto").val(monto_total);
}
function formatear_monto(amount, decimals){
	amount += ''; // por si pasan un numero en vez de un string

	amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

	decimals = decimals || 0; // por si la variable no fue fue pasada

	// si no es un numero o es igual a cero retorno el mismo cero
	if (isNaN(amount) || amount === 0) 
	 return parseFloat(0).toFixed(decimals);

	// si es mayor o menor que cero retorno el valor formateado como numero
	amount = '' + amount.toFixed(decimals);

	var amount_parts = amount.split('.'),
	 regexp = /(\d+)(\d{3})/;

	while (regexp.test(amount_parts[0]))
	 amount_parts[0] = amount_parts[0].replace(regexp, '$1' + '.' + '$2');

	return amount_parts.join(',');
}
function mostrar_new(elemento){
	$('#listado_ingreso').hide(500);
	$('#nuevo_ingreso').show(2000);
	//console.log($(elemento)[0].parentNode.className="active")
	$(elemento)[0].parentNode.previousElementSibling.className=""
	$(elemento)[0].parentNode.className="active"
}
function mostrar_listado(elemento){
	$('#nuevo_ingreso').hide(500);
	//console.log($(elemento)[0].parentNode.className="active")
	$(elemento)[0].parentNode.nextElementSibling.className=""
	$(elemento)[0].parentNode.className="active"
	$('#codigo').val('');
	$('#cantidad').val('');
	$('#precio').val('');
	$('#descripcion').val('');
	$('#fecha').val('');
	$('#monto').val('');
	$('#codigo_pagar').val('');
	$('#listado_ingreso').show(3000);
}
function mostrar_form(){
	$('#form_add').show(2000);
}
function cargar_conectados(){
	var lista = $('#lista_conects')
	var user_id = $('#user_id').val()
	lista[0].innerHTML=''
	$.get('/get_conect', function(data){
	  for(i in data){
	  	if(data[i].id!=user_id){
	  		var li = `<li  style="margin-top:1%;"><a href="#" onclick="charlar('${data[i].id}','${data[i].usuario}')" class="users_list"><span class="glyphicon glyphicon-user"></span> ${data[i].usuario}</a></li>`
	  		$(lista).append(li)
	  	}
	  	
	  } 
	})
}
function validar_mensaje(){
	if($('#mensaje').val()==''){
		$('.send-btn').attr("disabled", true);
	}else{
		$('.send-btn').attr("disabled", false);
	}
}
$('.em').on('click',function(){
	var emoji = $(this)[0].className
	if($('#mensaje').val()==''){
		$('.send-btn').attr("disabled", false);
		sessionStorage.setItem("emoji", emoji);
	}else{
		sessionStorage.setItem("emoji", emoji);
	}
})
function charlar(id,usuario){
	var lista = $('#lista_conects')
	var pchat = $('#private_chat')
	var lchat = $('.logo-chat')
	lchat.css('display','none')
	var user_chat = $('.user_chat')
	var lista_msg = $('#msgs')
	$('#other_id').val(id)
	var user_id = $('#user_id').val()
	var params = {from:user_id,to:id}
	$.ajax({
		headers: {
        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    	},
	    url:"/get_msg",
	    type:"POST",
	    dataType:"json",
	    data:params,
	    success: function(dato){
	    	user_chat[0].innerHTML=usuario
	    	lista_msg[0].innerHTML=''
	    	for(i in dato){
	    		if(dato[i].from==user_id){
					var li = `<li class="self" style="margin-top:1%;"><div class="msg">${dato[i].mensaje}</div><time>${dato[i].hora}</time></li>`
				}else{
					var li = `<li class="other" style="margin-top:1%;"><div class="msg">${dato[i].mensaje}</div><time>${dato[i].hora}</time></li>`
				}
				lista_msg.append(li)
	    	}
	    }
	});
	pchat.fadeIn(2000)
}
function send_msg(){
	var user_id = $('#user_id').val()
	var emoji = sessionStorage.getItem("emoji");
	var texto = $('#mensaje').val()
	if(emoji!=''){
		var mensaje = `${texto} <i class="${emoji}"></i>`
	}else{
		var mensaje = `${texto}`
	}
	
	var other_id = $('#other_id').val()
	var hora = moment().format('h:mm:ss')
	var fecha = moment().format('YYYY-MM-DD')
	var comentario = {from:user_id,to:other_id,mensaje:mensaje,hora:hora,fecha:fecha}
	$.ajax({
		headers: {
        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    	},
	    url:"/save_msg",
	    type:"POST",
	    dataType:"json",
	    data:comentario,
	    success: function(dato){
	    	if(dato==1){
	    		sessionStorage.removeItem('emoji');
	    		$('.send-btn').attr("disabled", true);
	    		socket.emit('enviar_comentario',comentario);
				$('#mensaje').val('')
	    	}
	    }
	});
}
socket.on('recibir_comentario',function(comentario){
	var user_id = $('#user_id').val()
	var lista_msg = $('#msgs')
	if(comentario.from==user_id){
		var li = `<li class="self" style="margin-top:1%;"><div class="msg">${comentario.mensaje}</div><time>${comentario.hora}</time></li>`
	}else{
		var li = `<li class="other" style="margin-top:1%;"><div class="msg">${comentario.mensaje}</div><time>${comentario.hora}</time></li>`
	}
	lista_msg.append(li)
	$("#msgs").animate({ scrollTop: $('#msgs')[0].scrollHeight}, 1000);
});
$(document).ready(function(){
    $(".chosen").chosen({no_results_text: "Lo sentimos, contacto no encontrado"});
    
});
