        $('.carousel').carousel({
            interval: 10000
        })
        $(function () {
            var $activate_scrollator1 = $('#activate_scrollator1');
            var $activate_scrollator2 = $('#activate_scrollator2');
            var $activate_scrollator3 = $('#activate_scrollator3');
            var $activate_scrollator4 = $('#activate_scrollator4');

            $activate_scrollator1.bind('click', function () {
                var $scrollable_div1 = $('#scrollable_div1');
                if ($scrollable_div1.data('scrollator') === undefined) {
                    $scrollable_div1.scrollator();
                    $activate_scrollator1.val('destroy scrollator')
                } else {
                    $scrollable_div1.scrollator('destroy');
                    $activate_scrollator1.val('activate scrollator')
                }
            });
            $activate_scrollator2.bind('click', function () {
                var $scrollable_div2 = $('#scrollable_div2');
                if ($scrollable_div2.data('scrollator') === undefined) {
                    $scrollable_div2.scrollator();
                    $activate_scrollator2.val('destroy scrollator')
                } else {
                    $scrollable_div2.scrollator('destroy');
                    $activate_scrollator2.val('activate scrollator')
                }
            });

            $activate_scrollator3.bind('click', function () {
                var $scrollable_div3 = $('#scrollable_div3');
                if ($scrollable_div3.data('scrollator') === undefined) {
                    $scrollable_div3.scrollator();
                    $activate_scrollator3.val('destroy scrollator')
                } else {
                    $scrollable_div3.scrollator('destroy');
                    $activate_scrollator3.val('activate scrollator')
                }
            });


            $activate_scrollator4.bind('click', function () {
                var $document_body = $('body');
                if ($document_body.data('scrollator') === undefined) {
                    $document_body.scrollator({
                        custom_class: 'body_scroller'
                    });
                    $activate_scrollator4.val('destroy scrollator')
                } else {
                    $document_body.scrollator('destroy');
                    $activate_scrollator4.val('activate scrollator')
                }
            });

            $activate_scrollator1.trigger('click');
            $activate_scrollator2.trigger('click');
            $activate_scrollator3.trigger('click');
            $activate_scrollator4.trigger('click');
        });

        $( document ).ready(function() {
          $("#loginn").attr('target','_blank');
        });

        $( "#loginn" ).click(function() {
          window.location.href = 'http://habemusecclesia.com/login';
        });
        $( "#mision-btn" ).click(function() {
          window.location.href = 'http://habemusecclesia.com/mision';
        });