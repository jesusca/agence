
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  
  <title>AGENCE</title>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
  <nav class="light-blue lighten-1" role="navigation">

    <div class="nav-wrapper container"><a id="logo-container" class="brand-logo"> 
      <img style="margin-top:15px;" alt="" src="/img/agence.png"></a>
      <ul class="right hide-on-med-and-down">
        <li ><a href="/login">Inicio</a></li>
        <li ><a href="#">Proyectos</a></li>
        <li ><a href="#">Administrativo</a></li>
        <li ><a href="/comercial"">Comercial</a></li>
        <li ><a href="#">Financiero</a></li>
        <li ><a href="#">Salir</a></li>
      </ul>

      <ul id="nav-mobile" class="side-nav">
       <li ><a href="/login">Inicio</a></li>
       <li ><a href="#">Proyectos</a></li>
       <li ><a href="#">Administrativo</a></li>
       <li ><a href="/comercial"">Comercial</a></li>
       <li ><a href="#">Financiero</a></li>
       <li ><a href="#">Salir</a></li>
     </ul>
     <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
   </div>
 </nav>
 <div class="section no-pad-bot" id="index-banner">
   
 </div>


 <div class="section no-pad-bot" id="index-banner">
  <div class="container">
    {!!Form::open(['url' => 'consulta_grafica','method'=>'POST'])!!}
    {!!Form::token()!!}

    <h5 class="header center black-text">LISTADO DE CONSULTORES</h5>
    <div class="row">
     <div class="col s6">
       <h7>Periodo desde:</h7>
       <input style="" placeholder="Username" name="desde" type="date" required="">
     </div>
     <div class="col s6">
      <h7>Periodo hasta:</h7>
      <input style="" placeholder="Username" name="hasta" type="date" required="">
    </div>
  </div>
  

  <div class="row center">
   <div class="col s12">
    <div class="mdl-selectfield">
      <select multiple="multiple" name="consultor[]" class="browser-default" required="">
        <option value="" disabled selected>Seleccione Consultor</option>
        @foreach($user as $key)  
        <option value="{{$key->co_usuario}}">{{$key->no_usuario}}</option>
        @endforeach 
      </select>
    </div>
  </div>
</div>

</div>
<div class="row center">
 <button name="boton1" type="submit" id="download-button" value="1" class="btn-large waves-effect waves-light orange">Relatório</button>
 <button name="boton1" type="submit" id="download-button" value="2" class="btn-large waves-effect waves-light orange">Gráfico</button>
 <button name="boton1" type="submit" id="download-button" value="3" class="btn-large waves-effect waves-light orange">Pizza</button>
</div>
<br><br>

</div>
</div>
{!!Form::close()!!}
</div>


<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="js/materialize.js"></script>
<script src="js/init.js"></script>

</body>
</html>
