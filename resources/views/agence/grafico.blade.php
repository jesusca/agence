<style type="text/css">



</style>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

  <title>AGENCE</title>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
  <nav class="light-blue lighten-1" role="navigation">

    <div class="nav-wrapper container"><a id="logo-container" class="brand-logo"> 
      <img style="margin-top:15px;" alt="" src="/img/agence.png"></a>
      <ul class="right hide-on-med-and-down">
        <li ><a href="/login">Inicio</a></li>
        <li ><a href="#">Proyectos</a></li>
        <li ><a href="#">Administrativo</a></li>
        <li ><a href="/comercial"">Comercial</a></li>
        <li ><a href="#">Financiero</a></li>
        <li ><a href="#">Salir</a></li>
      </ul>

      <ul id="nav-mobile" class="side-nav">
       <li ><a href="/login">Inicio</a></li>
       <li ><a href="#">Proyectos</a></li>
       <li ><a href="#">Administrativo</a></li>
       <li ><a href="/comercial"">Comercial</a></li>
       <li ><a href="#">Financiero</a></li>
       <li ><a href="#">Salir</a></li>
     </ul>
     <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
   </div>
 </nav>
 <div class="section no-pad-bot" id="index-banner">

 </div>


 <table id="table">
  <thead>
    <h5 style="text-align: center;">Informe por mes desde el día: <?php echo $desde; ?> hasta el día: <?php echo $hasta; ?></h5>
    <tr>
      <th></th>

      <th>Nombre del Consultor</th>
      <th>Receita Líquida</th>
      <th>Custo Fixo</th>
      <th>Comissão</th>
      <th>Lucro</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <?php $suma1=0; $suma2=0; $suma3=0; $suma4=0; ?>
      @foreach($query1 as $key) 
      @if ($key->no_usuario != null)
      <td data-title="ID"></td> 
      <td data-title="ID">{{$key->no_usuario}}</td>          
      <td>$ {{number_format($key->total,2, ',', '.')}}</td> 
      <td data-title="Link">$ {{number_format($key->brut_salario,2, ',', '.')}}</td>
      <td data-title="Link">$ {{number_format($key->otro,2, ',', '.')}}</td>
      <td data-title="Status">$ {{number_format($key->total-($key->brut_salario+$key->otro),2, ',', '.')}}</td>
      <td data-title="Link"></td>
    </tr>
    <?php $suma1+=$key->total; $suma2+=$key->brut_salario; 
    $suma3+=$key->otro; 
    $suma4+=$key->total-($key->brut_salario+$key->otro); ?>
    @endif
    @endforeach 


    <thead>
      <tr>
        <th></th>
        <th>TOTALES:</th>
        <th>{{number_format($suma1,2, ',', '.') }}</th>
        <th>{{number_format($suma2,2, ',', '.') }}</th>
        <th>{{number_format($suma3,2, ',', '.') }}</th>
        <th>{{number_format($suma4,2, ',', '.') }}</th>
        <th></th>
      </tr>
    </thead>

  </tr>
</tbody>
</table>

@if($query1==null)
<h5 style="text-align: center;">***NO HAY REGISTROS EN NUESTRA BASE DE DATOS***</h5>
@endif


<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="js/materialize.js"></script>
<script src="js/init.js"></script>

</body>
</html>
