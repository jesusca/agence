<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

  <title>AGENCE</title>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
  <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
  <script src="../../code/highcharts.js"></script>
  <script src="../../code/modules/exporting.js"></script>
  <nav class="light-blue lighten-1" role="navigation">

    <div class="nav-wrapper container"><a id="logo-container" class="brand-logo"> 
      <img style="margin-top:15px;" alt="" src="/img/agence.png"></a>
      <ul class="right hide-on-med-and-down">
        <li ><a href="/login">Inicio</a></li>
        <li ><a href="#">Proyectos</a></li>
        <li ><a href="#">Administrativo</a></li>
        <li ><a href="/comercial"">Comercial</a></li>
        <li ><a href="#">Financiero</a></li>
        <li ><a href="#">Salir</a></li>
      </ul>

      <ul id="nav-mobile" class="side-nav">
       <li ><a href="/login">Inicio</a></li>
       <li ><a href="#">Proyectos</a></li>
       <li ><a href="#">Administrativo</a></li>
       <li ><a href="/comercial"">Comercial</a></li>
       <li ><a href="#">Financiero</a></li>
       <li ><a href="#">Salir</a></li>
     </ul>
     <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
   </div>
 </nav>
 <div class="section no-pad-bot" id="index-banner">

 </div>
@if($query1==null)
<h5 style="text-align: center;">***NO HAY REGISTROS EN NUESTRA BASE DE DATOS***</h5>
@elseif($query1!=null)


 <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
 <script type="text/javascript">
  $(document).ready(function () {

    Highcharts.chart('container', {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
      },
      title: {
        text: 'AGENCE CONSULTORES'
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: false
          },
          showInLegend: true
        }
      },
      series: [{
        name: '% TOTAL',
        colorByPoint: true,
        data: [

        @foreach($query1 as $key) 

        {
          name: '{{$key->no_usuario}}  / T: ${{number_format($key->total,2, '.', '')}}' ,
          y: {{number_format($key->total,2, '.', '')}},
          sliced: true,
          selected: true
        },
        @endforeach
        ]
      }]
    });
  });
</script>
@endif
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="js/materialize.js"></script>
<script src="js/init.js"></script>

</body>
</html>
