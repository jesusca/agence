<style type="text/css">

</style>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

  <title>AGENCE</title>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
  <nav class="light-blue lighten-1" role="navigation">
    <script src="../../code/highcharts.js"></script>
    <script src="../../code/modules/data.js"></script>
    <script src="../../code/modules/exporting.js"></script>
    <div class="nav-wrapper container"><a id="logo-container" class="brand-logo"> 
      <img style="margin-top:15px;" alt="" src="/img/agence.png"></a>
      <ul class="right hide-on-med-and-down">
        <li ><a href="/login">Inicio</a></li>
        <li ><a href="#">Proyectos</a></li>
        <li ><a href="#">Administrativo</a></li>
        <li ><a href="/comercial"">Comercial</a></li>
        <li ><a href="#">Financiero</a></li>
        <li ><a href="#">Salir</a></li>
      </ul>

      <ul id="nav-mobile" class="side-nav">
       <li ><a href="/login">Inicio</a></li>
       <li ><a href="#">Proyectos</a></li>
       <li ><a href="#">Administrativo</a></li>
       <li ><a href="/comercial"">Comercial</a></li>
       <li ><a href="#">Financiero</a></li>
       <li ><a href="#">Salir</a></li>
     </ul>
     <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
   </div>
 </nav>
 <div class="section no-pad-bot" id="index-banner">
 </div>


<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
      
    

    <script type="text/javascript">
 @foreach($query1 as $key)  
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Agence Consultoria Informatica'
    },
    subtitle: {
        text: 'Source: WorldClimate.com'
    },
    xAxis: {
        categories: [
            'Enero',
            'Febrero',
            'Marzo',
            'Abril',
            'Mayo',
            'Junio',
            'Julio',
            'Agosto',
            'Septiembre',
            'Octubre',
            'Noviembre',
            'Diciembre'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Rainfall ()'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
          
        name: '{{$key->no_usuario}}',
        data: [{{ str_replace('"', ' ', $key->total) }} ]
    }]
}); 
    </script>
 @endforeach
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="js/materialize.js"></script>
<script src="js/init.js"></script>

</body>
</html>
